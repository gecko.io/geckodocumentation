# Gecko.io OSGi - Nice to Know
## Plain OSGi
### I want to use a LDAP style target filter as service property. How do I have to escape this filter string, that the org.osgi.framework.FrameworkUtil can create a valid Filter instance?

If you have a target filter like this *(foo=bar)*. In case you define that target filter as service property you get something like *myprop=(foo=bar)*. A corresponding LDAP like filter string would look like **(myprop=(foo=bar))**

Creating an *org.osgi.framework.Filter* out of this using the *org.osgi.framework.FrameworkUtil#createFilter* method will cause an *org.osgi.framework.InvalidSyntaxException*

If you create a filter like this:

`Filter f = FrameworkUtil.createFilter("(myprop=(foo=bar))");`

Apache Felix answers with an exception like this:

`org.osgi.framework.InvalidSyntaxException: Invalid value: (foo=bar)): (myprop=(foo=bar)) `

The correct escape is a reverse solidus *'\\'* . So a valid string would be:

`Filter f = FrameworkUtil.createFilter("(myprop=\\(foo=bar\\))");`

You find the syntax definition in the **OSGi Core Specification** in section [3.2.7](https://osgi.org/specification/osgi.core/7.0.0/framework.module.html#framework.module.filtersyntax)


### I want to use Component Annotations on a Component/Service which inherits from another class.

The Component Annotation of OSGi is only piced up and used by your tooling (namely bnd or the PDE) and generates a component XML file for you, which in turn is picked up by DS at runtime.

There is one inconvenience though. If you have a Reference Annotation on a field or method in your inherited class, the tooling will not recognize it. Our first solution looked as follows:

```java
@Component
public class InheritedTestService extends TestServiceImpl{

    @Reference
    public void setMyTestService(MyTestService testService){
        super.setMyTestService(testService);
    }

}

```

This is for one a useless piece of code and we would never have been able to perform e.g. field injection with this. The far better alternative would be:

```java
@Component(references = {
    @Reference(name = "testService", bindMethod = "setMyTestService")
})
public class InheritedTestService extends TestServiceImpl{

}
```
## bnd / bndtools

### I want to bring my gradle workspace to a new bnd version. How can I do that?

Usually you can use the workspace template of the IDE version of bnd. It`s no problem to create / update a workspace with a newer one. If you have own workspace templates, you should update them. No matter if for the template or a single project, you have to do this.

Use the template on the GitHub page: https://github.com/bndtools/workspace. *Note, that you have to select the right branch!*

To upgrade from bnd 4.1.0 with gradle 4.x to bnd 4.3.1 with gradle 5.x we touched the following files:

`gradle.properties`
This file contains the bnd version you want to use, in our case 4.3.1

`.gradle-wrapper/gradle-wrapper.properties`
This file contains the URL to the gradle packages. The distributions are usually available [here](https://services.gradle.org/distributions/)

`settings.gradle`
The build setup for bnd and the corresponding plugins.

You can take a look at the  `build.gradle` for changes.

If you update the gradle version, stop the gradle daemon calling `./gradlew --stop`, before using the newer version.


### I use bndtools for the Eclipse Plugin development. The E4 tools do not recognize my classes or files in bnd projects. E4 says that these projects are no OSGi projects.

This is because E4 tools look into the projects and expect a META-INF/MANIFEST.MF. In bnd, this file is usually generated and therefore only contained in the final artifact in the projects *generated* folder. 

An easy  workaround is to place an empty META-INF/MANIFEST.MF file in the project with at least a valid *Bundle-SymbolicName* declaration. This manifest will not be bundled in the jar by bnd, as long as you don`t include it in the *-includeresource* instruction

You can create a bnd project template for such projects, where the manifest is automatically created. Just take a look at the bndtools GitHub repository. There is the code for their default templates:

https://github.com/bndtools/project-templates/tree/master/org.bndtools.templates.osgi

### How can I trigger an export to a selfexecutable jar with gradle?

Run ```./gradlew export```.
If the Build was successful you will find the result in `generated/distribution/<yourBndrunFileName>.export` directory. Note, that the bndrun must reside in a bnd project with a bnd.bnd in it.

### Reading a failed Resolution Result or an unresolved OSGi requirement

Before we start: The Resolver works with the [Requirements and Capabilities](https://osgi.org/specification/osgi.core/7.0.0/framework.namespaces.html) of OSGi and I strongly suggest to have a look at them, if you work with the Resolver.

To give an Answer to the question above, we take the following example output:

```
Resolution failed. Capabilities satisfying the following requirements could not be found:
    [<<INITIAL>>]
      ⇒ osgi.identity: (osgi.identity=org.gecko.test.bundle.fragment)
          ⇒ [org.gecko.test.bundle.fragment version=1.0.0.SNAPSHOT]
              ⇒ osgi.wiring.host: (osgi.wiring.host=org.gecko.test.bundle.host)
                  ⇒ [org.gecko.test.bundle.host version=1.0.0]
                      ⇒ osgi.service: (objectClass=org.gecko.test.bundle.ApplicationConfiguration)
    [org.apache.aries.jpa.container version=2.7.0]
      ⇒ osgi.service: (objectClass=javax.persistence.spi.PersistenceProvider)
```

Lets take it apart and look what it is telling us:

```
Resolution failed. Capabilities satisfying the following requirements could not be found:
    [<<INITIAL>>]
```

Surprise! Something went wrong and the line after ```[<<INITIAL>>]``` represents one requirement given in the ```-runrequires``` that is the root cause for the current issue.

```
      ⇒ osgi.identity: (osgi.identity=org.gecko.test.bundle.fragment)
```

Bundle added via drag and drop and sometimes their versions are translated in OSGi Requirement filters for the [osgi.identity Namespace](https://osgi.org/specification/osgi.core/7.0.0/framework.namespaces.html#i1776750).

```
          ⇒ [org.gecko.test.bundle.fragment version=1.0.0.SNAPSHOT]
```

In between [] it will state the Bundle and Version found, that provides the requested [osgi.identity](https://osgi.org/specification/osgi.core/7.0.0/framework.namespaces.html#i1776750).

```
              ⇒ osgi.wiring.host: (osgi.wiring.host=org.gecko.test.bundle.host)
```

The Bundle found is a Fragment and the Resolver thus found a Requirement for a Host Bundle.

```
                  ⇒ [org.gecko.test.bundle.host version=1.0.0]
```

It also found a Host Bundle.

```
                      ⇒ osgi.service: (objectClass=org.gecko.test.bundle.MyService)
```

The Host-Bundle uses DS or CDI and has a mandatory service reference of the given type, represented by the [osgi.service Namepace](https://osgi.org/specification/osgi.cmpn/7.0.0/service.namespaces.html#service.namespaces-osgi.service.namespace). 

```
    [org.apache.aries.jpa.container version=2.7.0]
      ⇒ osgi.service: (objectClass=javax.persistence.spi.PersistenceProvider)
```

A part like this is often the confusing one. The indentation tells you, that it has nothing todo with the last requirement stated. This part can be longer and have multiple entries, but they can usually be ignored for the time being. 

The Important part was the last stated ```osgi.service``` Requirement. It tells us, that there is no bundle in your workspace or repositories, that states that it provides a service of the given type. This is the problem you need to work with and figure out why. The most common end in such a chain is ```osgi.identity``` if somebody uses ```Require-Bundle``` and the Bundle can't be found or ```osgi.wiring.package``` if no Bundle can be found providing such a package.

The most common cause for a missing service, is if you have a service referenced via DS or CDI, where BND automatically generates ```osgi.service``` Requirements for you. If the Service is registered via ```bundleContext.registerservice(...)```. In this case bnd is not picking this up and nobody provides such a requirement.  

To solve this you have two choices:

If the Bundle registering the service manually is your own, add the following Annotation to the registering class:

```java
@Capability(namespace = "osgi.service", attribute = "objectClass=org.gecko.test.bundle.MyService")
```

This will add the necessary Capability for you.

 If the service is registered by a third party Bundle, you can tell the resolver to ignore the ```osgi.service``` requirements. Please not, that this can cause problems at runtime, because there might be services missing, that you depend on. To do this add the ```-resolve.effective: active;skip:="osgi.service"``` instruction to your bndrun.  For more details look [here](https://bnd.bndtools.org/instructions/resolve.effective.html).

Fixing issues the resolver has, is a step by step process. Thus, after one issue is taken care of, you need to resolve again and tackle one problem at a time. 

### Running bndrun files using gradle

Bnd provides gradle tasks to run `bndrun` files and start an application. When listing all gradle tasks using `gradlew tasks`, you will find the bndrun tasks in the exports section of the list. Usually the tasks look like `run.<bndrun-file-name>`.

If you want to start you bndrun-application using gradle you can just call `./gradlew run.launch` to launch the `launch.bndrun`. 

Please note, that you may encounter an exception like this when running the gradle task:

```
Welcome to Apache Felix Gogo

g! gosh: stopping shell and framework
```

Please read the text below for information about this. 

### Using the Gogo Shell via Telnet

In certain situations Gogo breaks when starting the OSGi frameworks with:

```
Welcome to Apache Felix Gogo

g! gosh: stopping shell and framework
```

To avoid this, the Gogo shell can be deactivated using the JVM property `-Dgosh.args=--nointeractive`. You can also add the runproperty to the bndrun file `gosh.args=--nointeractive`

Alternatively the shell can be run as telnet service. Change the property value like this: `gosh.args='-sc telnetd -p7315 start'` to run the telnet daemon on port 7315. To connect to the shell, simply run `telnet <host> 7315`.

Please refer to the [Felix Gogo](http://felix.apache.org/documentation/subprojects/apache-felix-gogo.html)

### Start Ordering with Configuration Plugins in the Configuration Admin

Please read [this](felix-cm.md) or this [blog](https://www.datainmotion.de/osgi-configuration-admin-and-plugin-start-ordering)
