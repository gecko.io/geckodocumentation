# How to create the first Pull Request

When you want to contribute to a project but you are not allowed to immediately deploy your changes into the project, you need to get through a *pull request process.*

1. As first step you need to *fork* the repository you want to contribute to. A **fork** is a copy of a repository. Forking a repository allows you to freely experiment with changes without affecting the original project. Follow [these instructions](https://help.github.com/en/github/getting-started-with-github/fork-a-repo) to fork a repository in GitHub.

   

2. Once you have your forked version of the repository, you need to create a local clone of it. This is achieved by the **clone** command. The same instructions as before explain also this step in detail.

   

3. After you cloned the forked repository, make a new branch and then start making as many changes as you want. In this way you won't affect the original project.

   

4. When you are happy with your changes, you can commit and push them, as you are used to, to your forked repository.

   

5. At this stage the owner of the project has not yet been notified about your changes proposal. To do that, you need to make the actual *pull request.* Go to the gitHub page of the original project and look for the **New pull request** button. Select then from which fork and which branch you want the changes to be taken and then submit your pull request!



# How to make changes to your original Pull Request 

If your proposed changes to the original project are accepted by the project owner, then you did a very good job and you do not need to read further!

But, if the project owner is considering your proposal, but asked you to make some changes, then you would need to work a bit more.

Since the changes are still related to the same pull request you have made, there is no need to create another pull request. What you could do, instead, is to go back to your last commit, apply the required changes and re-push your work. To achieve that in Eclipse, do the following:

1. Switch to the **git** perspective

   

2. Right-click to the git repository and then click on **Reset**

   

3. Choose the remote tracking master as branch and mark the option which ensures a **SOFT** reset

   

4. Then you can apply the additional changes to the code and resubmit/re-push it

   

5. Once you have pushed it, you can check on the gitHub page of the original project, where the pull request should be automatically updated

