## How to add a Core Expression to make menu items visible only under certain conditions?

When developing our plug-in, we wanted to make the menu items we inserted visible only under certain conditions. For instance, one should be able to see the option to generate our `GeckoGenModel` file from the pop-up menu only if it opens it from a `.genmodel` file. To achieve this goal, Eclipse E4 makes at your disposal the possibility to add a `Core Expression`, which tells the model to make a certain item visible only if the requirement described in the expression are met. 

But how to add a custom `Core Expression` of this type? 

1. Open the `plugin.xml` file of your project and go the the `Extensions` tab;

2. Once there add a new extension of type `org.eclipse.core.expressions.definitions`

3. Add to it a `definition` and assign an *id* of your choice to such `definition`

4. Add then a `with` element and set the property *variable* to `selection`

5. Add an `iterate` element and set the *operator* to `and`

6. Then add an `adapt` element and as *type* put `org.eclipse.core.resoruces.IFile` (note that you can browse for such types, and the possibilities would depend on the dependencies you have, so check them before so you are sure to find what you are looking for)

7. Add a `test` element and set the *property* to `org.eclipse.core.resources.extension` and put as *value* the file extension for which you want your menu item to be enabled (in our case `genmodel`)

   ![](./images/coreExpression_0.png)

8. Then go to your `fragment.e4xmi` file, and in the menu item set the property *Visible-When-Expression* to `CoreExpression`. This will create a `Core Expression` element in your item

9. Go to the `Core Expression` element and set the *Expression-ID* to the one you set in step 3 (you should also be able to see it clicking on the *Find* button)