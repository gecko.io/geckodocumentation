# Create a Part through an E4 Frangment

We want to create a Part in an E4 Fragment plug-in.

1. In the `.e4xmi` add a `Model Fragment` with *Extended Element-ID* `org.eclipse.e4.legacy.ide.application` and *Feature Name* `descriptors`

   ![](./images/partThroughFragment_2.png)

   

2. Add to this fragment a `Part Descriptor` with an *ID* of your choice and link it to the actual Java class which will implement the view

   ![](./images/partThroughFragment_0.png)

   
 

3. Then click on the `Supplementary` tab of the `Part Descriptor` dialog, and, in the `Tags` section insert a new tag with value `View`

   ![](./images/partThroughFragment_1.png)

   

4. Open the `plugin.xml` file and go to the `Extensions` tab. There add a new extension of type `org.eclipse.ui.perspectiveExtensions`

   ![](./images/partThroughFragment_3.png)

   

5. Right-click on it and a add a `perspectiveExtension`

   ![](./images/partThroughFragment_4.png)

   

6. Then right-click on that and add a `view` element. In the `Extention Element Details` add as *id* the id of the `Part Descriptor` you have created in step 2. The *relative* option is the place where your view will be put (in our example it will be put on the bottom part of the Eclipse workspace, where the console is normally located)

   ![](./images/partThroughFragment_5.png)

   

   

