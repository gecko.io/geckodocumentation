## SWT_AWT Bridge with Scrolled Composite

When you want to include an AWT Frame into your SWT Composite, you can do it through the SWT_AWT bridge. In particular you can create an SWT Composite with the EMBEDDED style, like:

```java
Composite composite = new Composite(parent, SWT.EMBEDDED);
```

and then create the AWT Frame inside it, as:

```java
Frame awtFrame = SWT_AWT.new_Frame(composite);
```

So far so good. The problem appears when you want to add a scroll to your SWT composite.

The scroll won`t really work. What you can do is include the scroll in the AWT widgets, but this will look quite different from the standard SWT style. 

When looking for a possible solution, I came across this [nice post](https://www.eclipsezone.com/eclipse/forums/t26779.html), which provides a useful class  to implement a scrollable Swing area within an SWT Composite.

So, the only thing you need to do is to create a ScrollableSwingComposite, instead of the SWT embedded Composite:

```java
ScrollableSwingComposite composite = new ScrollableSwingComposite(parent, SWT.VERTICAL | SWT.HORIZONTAL);
```

This ScrollableSwingComposite comes with a JScrollPane, that you just have to retrieve for setting its viewpoint to the AWT base widget you want to wrap.

```java
JScrollPane scrollPane = composite.getJScrollPane();
scrollPane.setViewportView(yourAWTWidget);
```



 



