# How to open a Sirius session programmatically?

One of the step where we struggled the most was finding out how to properly launch a Sirius session programmatically. Some documentation exists, but it seemed we always missed something to make it properly work. At the end we were able to make it work by arranging all the pieces of information we found online. So, here is what we have done (to have all the context of our use case, please, visit [here](https://gitlab.com/gecko.io/geckoEMF-Tooling/blob/codegen-ilenia/org.gecko.emf.codegen.docs/tableOfContents.md)). 

1. Every time the `TreeViewer` with the Alloy Analyzer results is created we associate with it a new Sirius session, and we create a dedicated *session folder* (identified by a UUID) in the *.metadata* directory of the project, where all the output will be located. The code below is executed the first time the user click on a `TreeViewer` entry to display the Sirius diagram. The argument, in our case is the entry of the `TreeViewer`.

   

   ```java
   public AlloyDiagramPart(Alloy alloy) {
   UUID currentSessionID = UUID.randomUUID();		
   sessionFolderPath = "/"+alloy.getProjectFilePath()+"/.metadata/"+currentSessionID.toString()+"/";	
   representationURI = URI.createPlatformResourceURI(sessionFolderPath+"representations.aird", true);
   		
   Session session = SessionManager.INSTANCE.getExistingSession(representationURI);	
   		if(session == null) {
   			try {
   				currentSession = SessionFactory.INSTANCE.createSession(representationURI, new NullProgressMonitor());
   			} catch (CoreException e) {
   				logger.severe(String.format("Error creating Sirius session for file %s. %s", representationURI, e));
   			}
   		}
   		else {
   			currentSession = session;
   		}
   		logger.info(String.format("Starting Sirius Session. Output files are located in %s", sessionFolderPath));
   }
   ```

2. At this point we have our `Sirius Session` and also the location where to place the outputs relative to this session. Now we have to build the actual diagram. But first we close any open editors associated with this session (this is used when we click on subsequent entries of our `TreeViewer`, to ensure that the editor is replaced with the current one and to avoid opening a new editor every time an entry is selected). 

   

   ```java
   private Map<URI, IEditorPart> sessionMap = new HashMap<URI, IEditorPart>();
   
   private void closeEditor() {
   		IEditorPart openedEditor = sessionMap.get(representationURI);
   		if(openedEditor != null) {
   			openedEditor.doSave(new NullProgressMonitor());
   			if(DialectUIManager.INSTANCE.closeEditor(openedEditor, true)) {
   				sessionMap.put(representationURI, null);
   			}	
   		}				
   	}
   ```

   

3. Then we need first to add the URI of our Alloy resource to the semantic resources of the session.

   

   ```java
   currentAlloy = alloy;		
   ResourceSet resSet = new ResourceSetImpl();
   
   String fileName = alloy.getFilePath().substring(alloy.getFilePath().lastIndexOf("/")+1);
   
   String resName = fileName.substring(0, fileName.lastIndexOf(".")).concat(".alloyanalyzer");
   
   URI resURI = URI.createPlatformResourceURI(sessionFolderPath+resName, true);		
   saveResource(resSet, alloy, resURI);	
   
   AddSemanticResourceCommand cmd = new AddSemanticResourceCommand(currentSession, resURI, new NullProgressMonitor());
   		currentSession.getTransactionalEditingDomain().getCommandStack().execute(cmd);
   ```

   

4. We retrieve the `Viewpoint` we wanted to apply

   

   ```java
   UserSession userSession = UserSession.from(currentSession);
   Set<Viewpoint> viewpoints = ViewpointRegistry.getInstance().getViewpoints();
   Viewpoint viewpoint = getAlloyViewpoint(viewpoints);
   
   private Viewpoint getAlloyViewpoint(Collection<Viewpoint> viewpoints) {
   		for(Viewpoint vp : viewpoints) {
   			if(ALLOY_VIEWPOINT_NAME.equals(vp.getName())) {
   				return vp;
   			}
   		}
   		return null;
   	}
   
   if(viewpoint != null) {
   	userSession.selectViewpoint(ALLOY_VIEWPOINT_NAME);					
   	SessionManager.INSTANCE.add(currentSession);
   }
   ```

5. We launch a `RecordingCommand` in which we perform several things.

   1. We retrieve from the current session the semantic resource we want to represent 

      

      ```java
      Collection<Resource> sessionSemanticResources = currentSession.getSemanticResources();
      Resource currentSemanticResoruce = null;
      for(Resource resource : sessionSemanticResources) {							if(resURI.toString().equals(resource.getURI().toString())) {
      			currentSemanticResoruce = resource;
      			break;
      	}
      }
      if(currentSemanticResoruce == null) {
      	throw new IllegalStateException("Unable to retrieve semantic resource for Sirius session");
      }
      ```

   2. If this is found we also retrieve the selected `Viewpoint` and we retrieve ours

      

      ```java
      Collection<Viewpoint> selViewpoints = currentSession.getSelectedViewpoints(true);
      Viewpoint v = getAlloyViewpoint(selViewpoints);
      ```

   3. If this is also properly found, then we go through the `RepresentationDescription` and we look whether there is one which can create our semantic resource

      

      ```
      List<RepresentationDescription> descriptions = v.getOwnedRepresentations();
      for(RepresentationDescription description : descriptions) {				    if(DialectManager.INSTANCE.canCreate(currentSemanticResoruce.getContents().get(0), description)) { 
      		....
      }
      ```

   4. Now, since the representations of a session are not saved in the `representations.aird` till the session is closed, and we do not need to create every time a user clicks on the same entry a new representation for it, we have created a `Map<String, DRepresentaion>`, which stores, while a session is opened, all the representations which are created during it but still need to be saved. In such a way, if a representation is not found within the session saved representations, before creating a new one, we also check withing our map of representations. If no representation is found then we create a new one.

      

      ```java
      Collection<DRepresentation> existingRep = DialectManager.INSTANCE.getRepresentations(currentSemanticResoruce.getContents().get(0), currentSession);
      DRepresentation rep = null;
      //check in session saved representaions
      for(DRepresentation representation : existingRep) {
      	if(representation.getName().equals(resName)) {
      		rep = representation;
      		break;
      	}
      }									
      if(rep == null) {
          //check in not-yet-saved representations
      	for(DRepresentation sessionRep : sessionRepMap.values()) {
      		if(sessionRep.getName().equals(resName)) {
      			rep = sessionRep;
      			break;
      		}
      	}
      }
      //if still no right representation is found we create a new one
      if(rep == null) {
      	rep = DialectManager.INSTANCE.createRepresentation(resName, currentSemanticResoruce.getContents().get(0), description, currentSession, new NullProgressMonitor());
      }
      ```

   5. If the representation is correctly found or created, we open the editor associated with it, store it in the map of open editor and add some listeners to it in order to bind it with our `TreeViewer`.

      

      ```java
      if(rep != null) {
      	sessionRepMap.put(rep.getName(), rep);
      	IEditorPart currentEditor = DialectUIManager.INSTANCE.openEditor(currentSession, rep, new NullProgressMonitor());
      	if(currentEditor != null) {
      		sessionMap.put(representationURI, currentEditor);						addSiriusEditorListeners(currentEditor);							}
      	else {
      		logger.severe(String.format("Erorr opening Sirius editor for representation file %s", representationURI.toString()));
      	}
      }
      ```

      

